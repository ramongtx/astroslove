import ChatModel from "../Models/Chat";

export const ChatController = {
  index: async (req, res) => {
    try {
      const user = await ChatModel.find({ active: true });

      return res.status(200).json(user);
    } catch (error) {
      return res.status(400).json(error);
    }
  },
  store: async (req, res) => {
    try {
      const user = await ChatModel.create(req.body);

      return res.status(201).json(user);
    } catch (error) {
      return res.status(400).json(error);
    }
  },
  show: async (req, res) => {
    try {
      const user = await ChatModel.findById(req.params._id, {});

      return res.status(200).json(user);
    } catch (error) {
      return res.status(400).json(error);
    }
  },
  update: async (req, res) => {
    try {
      const user = await ChatModel.findByIdAndUpdate(req.params._id, req.body, {
        new: true
      });

      return res.status(200).json(user);
    } catch (error) {
      return res.status(400).json(error);
    }
  },
  destroy: async (req, res) => {
    try {
      const user = await ChatModel.findByIdAndUpdate(
        req.params._id,
        { active: false },
        { new: true }
      );

      return res.status(200).json(user);
    } catch (error) {
      return res.status(400).json(error);
    }
  },
  restore: async (req, res) => {
    try {
      const user = await ChatModel.findByIdAndUpdate(
        req.params._id,
        { active: true },
        { new: true }
      );

      return res.status(200).json(user);
    } catch (error) {
      return res.status(400).json(error);
    }
  }
};
