import UserModel, { IUser } from "../Models/User";
import ChatModel from "../Models/Chat";
import * as moment from "moment";

export const MatchController = {
  index: async (req, res) => {
    try {
      const matchs = await ChatModel.find({
        _users: { $includes: req.query._id }
      });
      res.status(200).json(matchs);
    } catch (error) {
      res.status(400).json(error);
    }
  },
  store: async (req, res) => {
    try {
      // Busca os 10 users mais próximos que não estao em user.matchs
      const user = await UserModel.findById(req.body._id);
      const matchs = await UserModel.find({
        $and: [
          { _id: { $not: user._id } },
          { _id: { $not: { $include: user.matchs } } }
        ],
        active: true
        //busca os mais proximos
      })
        .limit(10)
        .lean();
      // acessa a API e faz o score
      // Escolhe o que tem maior score
      let bestUser: IUser;
      for (const match of matchs) {
      }
      // Adiciona o user._id em user.matchs
      user.matchs.push({
        _user: bestUser._id,
        createdAt: moment().toDate()
      });
      await user.save();
      // Cria o chat
      const chat = await ChatModel.create({
        _users: [user._id, bestUser._id]
      });

      res.status(201).json({ chat, bestUser });
    } catch (error) {
      res.status(400).json(error);
    }
  },
  show: async (req, res) => {
    try {
      const chat = await ChatModel.findById(req.query._chat);
      res.status(200).json(chat);
    } catch (error) {
      res.status(400).json(error);
    }
  },
  update: async (req, res) => {},
  destroy: async (req, res) => {},
  restore: async (req, res) => {}
};
