import { model, Document, Schema, Types } from "mongoose";

export interface IUser extends Document {
  email: string;
  password: string;
  code: number;
  name: string;
  photo: string;
  bornAt: Date;
  bornIn: {
    lat: number;
    lng: number;
  };
  locate: {
    coordenates: Array<number>;
    type: string;
  };
  birthChart: {
    sign: string;
    resingSgn: string;
    houses: Array<string>;
    planets: Array<string>;
  };
  matchs: Array<{
    _user: Types.ObjectId;
    createdAt: Date;
  }>;
  active: boolean;
}

const UserSchema: Schema = new Schema(
  {
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    code: { type: Number },
    name: { type: String },
    photo: { type: String },
    bornAt: { type: Date },
    bornIn: {
      lat: Number,
      lng: Number
    },
    locate: {
      coordenates: { type: [Number] },
      type: { type: String }
    },
    birthChart: {
      sign: { type: String },
      resingSgn: { type: String },
      houses: { type: [String] },
      planets: { type: [String] }
    },
    matchs: [
      {
        _user: { type: Types.ObjectId },
        createdAt: { type: Date }
      }
    ],
    active: { type: Boolean, default: true }
  },
  { timestamps: true }
);

export default model<IUser>("User", UserSchema);
