import { model, Document, Schema, Types } from "mongoose";

export interface IChat extends Document {
  _users: Array<Types.ObjectId>;
  messages: Array<{
    _user: Types.ObjectId;
    text: string;
    sendAt: Date;
  }>;
  matchScore: number;
  active: boolean;
}

const ChatSchema: Schema = new Schema(
  {
    _users: [{ type: Types.ObjectId }],
    messages: [
      {
        _user: { type: Types.ObjectId },
        text: { type: String },
        sendAt: { type: Date }
      }
    ],
    matchScore: { type: Number },
    active: { type: Boolean, default: true }
  },
  { timestamps: true }
);

export default model<IChat>("Chat", ChatSchema);
