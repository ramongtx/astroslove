import * as express from "express";
import * as mongoose from "mongoose";
import * as cors from "cors";
import { router } from "./routes";

const app = express();

app.use(cors());
app.use(express.json());

app.use("/", router);

let MONGO_URI =
  "mongodb+srv://astrolove:astrolove@astrolove-v8big.mongodb.net/test?retryWrites=true&w=majority";

const PORT = process.env.PORT || 3333;

mongoose.connect(MONGO_URI, { useNewUrlParser: true }, err => {
  if (err) {
    console.log("MongoDB:", err);
    throw Error("MongoDB not connect");
  }

  console.log("Connect MongoDB");

  app.listen(PORT, () => {
    console.log(`Server is running in http://localhost:${PORT}`);
  });
});
