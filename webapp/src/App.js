import React, { Component } from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import LoginComponent from "./pages/Login";
import RegisterComponent from "./pages/Register";
import DashboardComponent from "./pages/Dashboard";

import store from "./store";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={LoginComponent} />
            <Route path="/cadastrar/" component={RegisterComponent} />
            <Route path="/dashboard/" component={DashboardComponent} />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}
