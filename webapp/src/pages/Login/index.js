import React, { useState } from "react";

import userProvider from "../../providers/user";

import "./style.css";

const LoginComponent = props => {
  const _id = localStorage.getItem("userId");
  if (_id) {
    props.history.push("/dashboard");
  }

  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");

  const changeEmail = event => setEmail(event.target.value);

  const changePass = event => setPass(event.target.value);

  const login = async event => {
    event.preventDefault();

    const response = await userProvider.login(email, pass);
    if (response.data) {
      localStorage.setItem("userId", response.data._id);
      props.history.push("/dashboard");
    }
  };

  return (
    <div className="container max-height">
      <div className="col col-center">
        <img
          src="http://luciodesigner.com.br/wp-content/uploads/2017/07/tamanho-logo-300x143.png"
          alt="Logo"
        />
      </div>
      <div className="col col-center">
        <form onSubmit={login}>
          <input placeholder="e-mail" onChange={changeEmail} />
          <input
            placeholder="senha pessoal"
            type="password"
            onChange={changePass}
          />
          <button tyoe="submit">Entrar</button>
          <a href={"/cadastrar"}>cadastrar-se</a>
        </form>
      </div>
    </div>
  );
};

export default LoginComponent;
